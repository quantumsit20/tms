<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmloyeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emloyees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('fname');
            $table->string('lname');
            $table->string('address');
            $table->text('phone');
            $table->text('mobile');
            $table->string('email')->unique();
            $table->text('job');
            $table->string('gender');
            $table->date('birthday');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emloyees');
    }
}
