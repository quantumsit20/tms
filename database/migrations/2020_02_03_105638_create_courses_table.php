<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
             $table->bigIncrements('id');
             $table->string('en_title');
             $table->string('ar_title');
             $table->string('type');
             $table->string('status');
             $table->date('start_date');
             $table->date('end_date');
             $table->date('duration');
             $table->integer('student_enrolled');
             $table->integer('price');
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
