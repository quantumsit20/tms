<div class="header-bottom hidden-tablet-landscape" id="js-navbar-fixed">
    <div class="container">
        <div class="header-bottom">
            <div class="header-bottom-content display-flex">
                <div class="logo">
                    <a href="index2.html">
                        <img src="{{asset('frontend/images/logo.png')}}" alt="SmartEdu">
                    </a>
                </div>
                <div class="menu-search display-flex">
                    <nav class="menu">
                        <div>
                            <ul class="menu-primary">
                                <li class="menu-item curent-menu-item">
                                    <a href="#">Home</a>
                                </li>
                                <li class="menu-item">
                                    <a href="courses.html">Courses</a>
                                </li>
                                <li class="menu-item">
                                    <a href="aboutus.html"> About us </a>
                                </li>
                                <li class="menu-item">
                                    <a href="events.html">Events</a>
                                </li>
                                <li class="menu-item">
                                    <a href="blog3.html">News</a>
                                </li>
                                <li class="menu-item">
                                    <a href="contact3.html">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>