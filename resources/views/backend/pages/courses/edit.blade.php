@extends('backend.layouts.master')
@section('title',__('tr.Update course'))
@section('stylesheet')
@endsection
@section('content')


    <!--begin::Portlet-->
    <div class="m-portlet">


        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>

                    <h3 class="m-portlet__head-text">

                        @lang('tr.Update course')
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <a href="{{ route('courses') }}" class="btn btn-secondary m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
													<span>
														<i class="la la-arrow-left"></i>
														<span>@lang('tr.Back to list')</span>
													</span>
                </a>


            </div>

        </div>

        <!--begin::Form-->
        @include('backend.components.errors')
        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="{{ route('update_courses',$courses->id) }}" method="post" enctype="multipart/form-data">
            @csrf
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6">
                                <label>@lang('tr.en_title'):</label>
                                <input type="text" name="en_title" id="en_title"  value="{{ $courses->en_title }}" class="form-control m-input">

                            </div>

                    <div class="col-lg-6">
                        <label>@lang('tr.ar_title'):</label>
                        <input type="text" name="ar_title" id="ar_title" value="{{ $courses->ar_title }}" class="form-control m-input" >

                    </div>
                    <div class="col-lg-6">
                        <label>@lang('tr.type'):</label>
                        <input type="text" name="type" id="type"  value="{{ $courses->type }}" class="form-control m-input">

                    </div>

                    <div class="col-lg-6">
                        <label class="">@lang('tr.status'):</label>
                        <input type="text" name="status" id="status" value="{{ $courses->status }}"class="form-control m-input" >

                    </div>



                </div>
                <div class="form-group m-form__group row">

                    <div class="col-lg-6">
                        <label>@lang('tr.instructors'):</label>
                        <input type="text" name="instructor_id" id="instructor_id" value="{{ $courses->instructor_id }}"class="form-control m-input" >


                    </div>









                <div class="col-lg-4">
                    <label>@lang('tr.start_date'):</label>
                    <div class="m-input-icon m-input-icon--right">
                        <input type="date" name="start_date" id="start_date" value="{{ $courses->start_date }}"  class="form-control m-input">

                    </div>

                </div>

            </div>

            <div class="col-lg-4">
                <label>@lang('tr.duration'):</label>
                <div class="m-input-icon m-input-icon--right">
                    <input type="date" name="duration" id="duration" value="{{ $courses->duration }}"  class="form-control m-input">

                </div>

            </div>

    </div>


    <div class="col-lg-4">
        <label>@lang('tr.end_date'):</label>
        <div class="m-input-icon m-input-icon--right">
            <input type="date" name="end_date" id="end_date" value="{{ $courses->end_date }}"  class="form-control m-input">

        </div>

    </div>






            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-8">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-edit"></i>&nbsp;@lang('tr.Update')
                            </button>

                        </div>
                    </div>
                </div>
            </div>
        </form>

        <!--end::Form-->
    </div>

    <!--end::Portlet-->

@endsection

@section('javascript')

