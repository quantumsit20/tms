@extends('backend.layouts.master')
@section('title',__('tr.show course'))
@section('stylesheet')

@endsection
@section('content')

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">

                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--last m-portlet--head-lg m-portlet--responsive-mobile" id="main_portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-progress">

                            <!-- here can place a progress bar-->
                        </div>
                        <div class="m-portlet__head-wrapper">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
													<span class="m-portlet__head-icon">
														<i class="flaticon-map-location"></i>
													</span>
                                    <h3 class="m-portlet__head-text">
                                        @lang('tr.show course')
                                    </h3>
                                </div>
                            </div>
                            <div class="m-portlet__head-tools">
                                <a href="{{ route('courses') }}" class="btn btn-secondary m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
													<span>
														<i class="la la-arrow-left"></i>
														<span>Back</span>
													</span>
                                </a>


                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <form class="m-form m-form--label-align-left- m-form--state-" id="m_form">

                            <!--begin: Form Body -->
                            <div class="m-portlet__body">
                                <div class="row">
                                    <div class="col-xl-8 offset-xl-2">
                                        <div class="m-form__section m-form__section--first">
                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">@lang('courses Details')</h3>
                                            </div>

                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">*@lang('instructor_id'):</label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <input type="text" name="title" class="form-control m-input" value="{{ $courses->instructor_id}}">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">*@lang('title'):</label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <input type="text" name="title" class="form-control m-input" value="{{ $courses->en_title }}">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">*status:</label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <input type="text" name="status" class="form-control m-input" placeholder="" value="{{ $courses->status }}">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">* type</label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <div class="input-group">

                                                        <input type="text" name="type" class="form-control m-input" placeholder="" value="{{ $courses->type }}">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                        <div class="m-separator m-separator--dashed m-separator--lg"></div>
                                        <div class="m-form__section">
                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">
                                                    @lang('More Details')
                                                    <i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon flaticon-info" title="Some help text goes here"></i>
                                                </h3>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">* duration:</label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <input type="date" name="duration" class="form-control m-input" placeholder="" value="{{ $courses->duration }}">

                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">end_date:</label>
                                                <div class="col-xl-9 col-lg-9">
                                                    <input type="date" name="end_date" class="form-control m-input" placeholder="" value="{{ $courses->end_date }}">

                                                </div>
                                            </div>



                                        </div>
                                        <div class="m-separator m-separator--dashed m-separator--lg"></div>


                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <!--end::Portlet-->
            </div>
        </div>
    </div>
@endsection



@section('javascript')


@endsection