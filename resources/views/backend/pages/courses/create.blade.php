@extends('backend.layouts.master')
@section('title',__('tr.Create New course'))

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">

        <div class="m-portlet__head">
            <div class="m-portlet__head-progress">

                <!-- here can place a progress bar-->
            </div>
            <div class="m-portlet__head-wrapper">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
													<span class="m-portlet__head-icon">
														<i class="flaticon-map-location"></i>
													</span>
                        <h3 class="m-portlet__head-text">
                            @lang('tr.Create New course')
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <a href="{{ route('courses') }}" class="btn btn-secondary m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
													<span>
														<i class="la la-arrow-left"></i>
														<span>Back</span>
													</span>
                    </a>


                </div>
            </div>
        </div>

        <!--begin::Form-->
        @include('backend.components.errors')
        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="{{ route('store_courses') }}" method="post" enctype="multipart/form-data" id="selectform">
            @csrf
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6">
                        <label>@lang('tr.instructors'):</label>
                        <select name="instructor_id" id="instructor_id" class="form-control" required>
                            <option value="">@lang('tr.Select instructors')</option>
                            @foreach ($instructors as $instructors)
                                <option value="{{ $instructors->id }}">{{$instructors->fname }}</option>
                            @endforeach
                        </select>

                    </div>

                    <div class="col-lg-6">
                        <label>@lang('tr.en_title'):</label>
                        <input type="text" name="en_title" id="en_title" class="form-control m-input" placeholder="@lang('tr.Enter en_title')">
                        <span class="m-form__help">Please Enter en_title</span>
                    </div>
                    <div class="col-lg-6">
                        <label>@lang('tr.ar_title'):</label>
                        <input type="text" name="ar_title" id="ar_title" class="form-control m-input" placeholder="@lang('tr.Enter ar_title')">
                        <span class="m-form__help">Please Enter ar_title</span>
                    </div>

                    <div class="col-lg-6">
                        <label class="">@lang('tr.type'):</label>
                        <input type="text" name="type" id="type" class="form-control m-input" placeholder="@lang('tr.Enter your type')">
                        <span class="m-form__help">Please Enter type</span>
                    </div>



                </div>
                <div class="form-group m-form__group row">

                    <div class="col-lg-4">
                        <label>@lang('tr.status'):</label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" name="status" id="status" class="form-control m-input" placeholder="@lang('tr.Enter your status')">
                        </div>
                        <span class="m-form__help">Please Enter your status</span>
                    </div>
                    <div class="col-lg-4">
                        <label>@lang('tr.price'):</label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" name="price" id="price" class="form-control m-input" placeholder="@lang('tr.Enter your price')">
                        </div>
                        <span class="m-form__help">Please Enter your price</span>
                    </div>


                <div class="form-group m-form__group row">
                    <div class="col-lg-4">
                        <label class="">@lang('tr.start_date'):</label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="date" name="start_date" id="start_date" class="form-control m-input" placeholder="@lang('tr.Enter your start_date')">
                        </div>
                        <span class="m-form__help">Please Enter your Job</span>
                    </div>


                    <div class="col-lg-4">
                        <label class="">@lang('tr.duration'):</label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="date" name="duration" id="duration" class="form-control m-input" placeholder="@lang('tr.Enter your duration')">
                        </div>
                        <span class="m-form__help">Please Enter your duration</span>
                    </div>



                    <div class="col-lg-4">
                        <label class="">@lang('tr.end_date'):</label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="date" name="end_date" id="end_date" class="form-control m-input" placeholder="@lang('tr.Enter your end_date')">
                        </div>
                        <span class="m-form__help">Please Enter your end_date</span>
                    </div>

                </div>


            </div>




            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-8">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-save"></i>&nbsp;@lang('tr.save')
                            </button>
                            <button type="reset" class="btn btn-accent reset">
                                <i class="la la-undo"></i>@lang('tr.reset')</button>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </form>

        <!--end::Form-->
    </div>

    <!--end::Portlet-->

@endsection

@section('javascript')

    <script>

        // Get all the reset buttons from the dom
        var resetButtons = document.getElementsByClassName('reset');

        // Loop through each reset buttons to bind the click event
        for(var i=0; i<resetButtons.length; i++){
            resetButtons[i].addEventListener('click', resetForm);
        }

        /**
         * Function to hard reset the inputs of a form.
         *
         * @param object event The event object.
         * @return void
         */
        function resetForm(event){

            event.preventDefault();

            var form = event.currentTarget.form;
            var inputs = form.querySelectorAll('input');

            inputs.forEach(function(input, index){
                input.value = null;
            });

        }
    </script>