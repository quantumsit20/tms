@extends('backend.layouts.master')
@section('title',__('tr.Update employee'))
@section('stylesheet')
@endsection
@section('content')


    <!--begin::Portlet-->
    <div class="m-portlet">


        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>

                    <h3 class="m-portlet__head-text">

                        @lang('tr.Update employee')
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <a href="{{ route('employees') }}" class="btn btn-secondary m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
													<span>
														<i class="la la-arrow-left"></i>
														<span>@lang('tr.Back to list')</span>
													</span>
                </a>


            </div>

        </div>

        <!--begin::Form-->
        @include('backend.components.errors')
        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="{{ route('update_employees',$employees->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <div class="col-lg-6">
                        <label>@lang('tr.Code'):</label>
                        <input type="text" name="code" id="code"  value="{{ $employees->code }}" class="form-control m-input">

                    </div>

                    <div class="col-lg-6">
                        <label>@lang('tr.Fname'):</label>
                        <input type="text" name="fname" id="fname" value="{{ $employees->fname }}" class="form-control m-input" >

                    </div>
                    <div class="col-lg-6">
                        <label>@lang('tr.Lname'):</label>
                        <input type="text" name="lname" id="lname"  value="{{ $employees->lname }}" class="form-control m-input">

                    </div>

                    <div class="col-lg-6">
                        <label class="">@lang('tr.Email'):</label>
                        <input type="email" name="email" id="email" value="{{ $employees->email }}"class="form-control m-input" >

                    </div>



                </div>
                <div class="form-group m-form__group row">

                    <div class="col-lg-4">
                        <label>@lang('tr.Adress'):</label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" name="address" id="address"  value="{{ $employees->address }}" class="form-control m-input">
                            <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
                        </div>

                    </div>



                    <div class="col-lg-4">
                        <label>@lang('tr.Mobile'):</label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" name="mobile" id="mobile" value="{{ $employees->mobile }}" class="form-control m-input">

                        </div>

                    </div>


                    <div class="col-lg-4">
                        <label>@lang('tr.Birthday'):</label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="date" name="birthday" id="birthday" value="{{ $employees->birthday }}"  class="form-control m-input">

                        </div>

                    </div>

                </div>



                <div class="form-group m-form__group row">
                    <div class="col-lg-6">
                        <label class="">@lang('tr.Job'):</label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" name="job" id="job" value="{{$employees->job }}" class="form-control m-input" >
                        </div>

                    </div>
                    <div class="col-lg-4">
                        <label class="">@lang('tr.Gender'):</label>
                        <div class="m-radio-inline">
                            <label class="m-radio m-radio--solid">
                                <input type="radio" name="gender" disabled value="male"{{$employees->gender == 'male' ? 'checked' : ''}}>@lang('tr.Male')

                                <span></span>
                            </label>
                            <label class="m-radio m-radio--solid">
                                <input type="radio" name="female"  value="female"{{$employees->gender == 'female' ? 'checked' : ''}}>@lang('tr.Female')

                                <span></span>
                            </label>
                        </div>

                    </div>
                </div>


            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-8">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-edit"></i>&nbsp;@lang('tr.Update')
                            </button>

                        </div>
                    </div>
                </div>
            </div>
        </form>

        <!--end::Form-->
    </div>

    <!--end::Portlet-->

@endsection

@section('javascript')

