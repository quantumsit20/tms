<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\emloyee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = emloyee::all();
        return view('backend.pages.employees.index',compact('employees'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    $request->validate([
              'fname'   => 'required|max:255|min:5',
              'address' => 'required|max:255|min:5',
              'job'     => 'required',
              'lname'   => 'required|max:255|min:5',
              'mobile'  => 'required|max:255|min:2',
              'email'   => 'required|unique:emloyees',
              'code'    => 'required|numeric',
              'gender'  => 'required',
              'birthday'  =>  'required|date',
        ]);

        $employee = new emloyee();

        $employee->code    = $request->code;
        $employee->fname   = $request->fname;
        $employee->lname   = $request->lname;
        $employee->email   = $request->email;
        $employee->address = $request->address;
        $employee->phone   = $request->phone;
        $employee->mobile  = $request->mobile;
        $employee->job     = $request->job;
        $employee->gender  = $request->gender;
        $employee->birthday  = $request->birthday;
        $employee->save();

        return redirect()->route('employees')->with('success',__('tr.employee Added successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $employee = emloyee::findOrfail($id);
        return view('backend.pages.employees.show',compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employees = emloyee::findOrfail($id);
        return view('backend.pages.employees.edit',compact('employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employees = emloyee::findOrfail($id);

        $request->validate([
            'fname'   => 'required|max:255|min:5',
            'address' => 'required|max:255|min:5',
            'job'     => 'required',
            'lname'   => 'required|max:255|min:5',
            'mobile'  => 'required|max:255|min:2',
            //'email'   => 'required|unique:emloyees',
            'code'    => 'required|numeric',
            'gender'  => 'required',
            'birthday'  =>  'required|date',
        ]);


        $employees->code    = $request->code;
        $employees->fname   = $request->fname;
        $employees->lname   = $request->lname;
        $employees->email   = $request->email;
        $employees->address = $request->address;
        $employees->phone   = $request->phone;
        $employees->mobile  = $request->mobile;
        $employees->job     = $request->job;
        $employees->gender  = $request->gender;
        $employees->birthday  = $request->birthday;
        $employees->save();


        return redirect()->route('employees')->with('success',__('tr.employee Updated successfully'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $employees = emloyee::findOrfail($id);
        $employees->delete();

        return redirect()->route('employees')->with('success',__('tr.employee Deleted successfully'));
    }
}
