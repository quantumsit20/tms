<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\instructor;
use App\Models\course;
use Illuminate\Support\Facades\Input as input;
use DB;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses= course::all();
        $instructors = instructor::all();

       return view('backend.pages.courses.index',compact('courses','instructors'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lang = \Lang::getLocale();
        $instructors = instructor::all();
        $courses = course::select($lang.'_title as title','type','status','start_date','end_date','instructor_id','duration','price')->get();

        return view('backend.pages.courses.create',compact('courses','instructors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'en_title'         => 'required',
            'ar_title'         => 'required',
            'status'           => 'required',
            'type'             => 'required',
            'start_date'       => 'required',
            'duration'         => 'required',
            'end_date'         => 'required',
            'price'            => 'required',
        ]);

        
        $courses = new course();
        $instructors = instructor::findOrfail($request->instructor_id);

        $courses->instructor_id = $request->instructor_id;
        $courses->en_title = $request->en_title;
        $courses->ar_title = $request->ar_title;
        $courses->status = $request->status;
        $courses->type = $request->type;
        $courses->start_date = $request->start_date;
        $courses->duration = $request->duration;
        $courses->end_date = $request->end_date;
        $courses->price = $request->price;
        $courses->save();

        return redirect()->route('courses')->with('success',__('tr.course Added successfully'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$instructors = instructor::findOrfail($id);
        $courses = course::findOrfail($id);
        return view('backend.pages.courses.show',compact('courses'));

//           $instructors = instructor::findOrfail($id);
//           $course = course::where('instructors_id','=', $instructors->id)
//            ->join('instructors','instructors.id', '=', 'courses.instructor_id')
//            ->get(array('courses.id as id','instructors.fname as fname','courses.status as status','courses.type as type','courses.start_date as start_date','courses.end_date as end_date','courses.duration as duration'));
//           return view('backend.pages.courses.show',compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $courses = course::findOrfail($id);
        $instructors = instructor::all();
        return view('backend.pages.courses.edit',compact('courses','instructors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $courses = course::findOrfail($id);
        $request->validate([
            'en_title'         => 'required',
            'ar_title'         => 'required',
            'status'           => 'required',
            'type'             => 'required',
            'start_date'       => 'required',
            'duration'         => 'required',
            'end_date'         => 'required',
            'price'            => 'required',
        ]);


        $instructors = instructor::findOrfail($request->instructor_id);

        $courses->instructor_id = $request->instructor_id;
        $courses->en_title = $request->en_title;
        $courses->ar_title = $request->ar_title;
        $courses->status = $request->status;
        $courses->type = $request->type;
        $courses->start_date = $request->start_date;
        $courses->duration = $request->duration;
        $courses->end_date = $request->end_date;
        $courses->price = $request->price;
        $courses->save();

        return redirect()->route('courses')->with('success',__('tr.course Updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $courses = course::findOrfail($id);
        $courses->delete();
        return redirect()->route('courses')->with('success',__('tr.courses Deleted'));
    }
}
