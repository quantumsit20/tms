<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\instructor;

class InstructorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instructors = instructor::all();
        return view('backend.pages.instructors.index',compact('instructors'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.instructors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'code'           =>  'required|numeric',
            'fname'          =>  'required|max:255|min:5',
            'lname'          =>  'required|max:255|min:5',
            'address'        =>  'required|max:255|min:5',

            'mobile'         =>  'required|max:255|min:2',
            'phone'          =>  'required|max:255|min:2',
            'email'          =>  'required|unique:instructors',
            'gender'         =>  'required',
            'birthday'       =>  'required|date',
            'avilable_time'  =>  'required',
            'shortbio'       =>  'required',
            'salary'         =>  'required|numeric',

        ]);

        $instructor = new instructor();

        $instructor->code    = $request->code;
        $instructor->fname   = $request->fname;
        $instructor->lname   = $request->lname;
        $instructor->email   = $request->email;
        $instructor->address = $request->address;
        $instructor->phone   = $request->phone;
        $instructor->mobile  = $request->mobile;

        $instructor->gender  = $request->gender;
        $instructor->birthday  = $request->birthday;
        $instructor->avilable_time  = $request->avilable_time;
        $instructor->shortbio  = $request->shortbio;
        $instructor->salary  = $request->salary;
        $instructor->save();

        return redirect()->route('instructors')->with('success',__('tr.instructor Added successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $instructors = instructor::findOrfail($id);
        return view('backend.pages.instructors.show',compact('instructors'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $instructors = instructor::findOrfail($id);
        return view('backend.pages.instructors.edit',compact('instructors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $instructors = instructor::findOrfail($id);

        $request->validate([
            'code'           =>  'required|numeric',
            'fname'          =>  'required|max:255|min:5',
            'lname'          =>  'required|max:255|min:5',
            'address'        =>  'required|max:255|min:5',

            'mobile'         =>  'required|max:255|min:2',
            'phone'          =>  'required|max:255|min:2',
            'email'          =>  'required|unique:instructors',
            'gender'         =>  'required',
            'birthday'       =>  'required|date',
            'avilable_time'  =>  'required',
            'shortbio'       =>  'required',
            'salary'         =>  'required|numeric',
        ]);



        $instructors->code    = $request->code;
        $instructors->fname   = $request->fname;
        $instructors->lname   = $request->lname;
        $instructors->email   = $request->email;
        $instructors->address = $request->address;
        $instructors->phone   = $request->phone;
        $instructors->mobile  = $request->mobile;

        $instructors->gender  = $request->gender;
        $instructors->birthday  = $request->birthday;
        $instructors->avilable_time  = $request->avilable_time;
        $instructors->shortbio  = $request->shortbio;
        $instructors->salary  = $request->salary;
        $instructors->save();

        return redirect()->route('instructors')->with('success',__('tr.instructors Updated successfully' ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $instructors = instructor::findOrfail($id);
        $instructors->delete();
        return redirect()->route('instructors')->with('success',__('tr.instructors Deleted'));
    }
}
