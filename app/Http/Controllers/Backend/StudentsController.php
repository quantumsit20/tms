<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\student;
use App\Models\course_student;
use DB;


class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $student = DB::table('courses')
            ->join('course_students','course_students.course_id', '=', 'courses.id')
            ->join('students','students.id', '=', 'course_students.student_id')
            ->get(array('students.id as id', 'students.fname as fname',
                'courses.en_title as en_title'))->toArray();
        return view('backend.pages.students.index',compact('student'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $students = student::all();
        $course = course::all();
        return view('backend.pages.courses.create',compact('students','course'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'fname'                    => 'required',
            'lname'                    => 'required',
            'address'                  => 'required',
            'phone'                    => 'required',
            'email'                    => 'required|unique:students',
            'status'                   => 'required',
            'gender'                   => 'required',
            'dateofjoin'               => 'required',
            'amount_paid'              => 'required',
            'remaining_amount'         => 'required',

        ]);


        $students = new student();
        $courses = course::findOrfail($request->id);

        $students->course_id = $request->course_id;
        $students->fname = $request->fname;
        $students->lname = $request->lname;
        $students->address = $request->address;
        $students->phone = $request->mobile;
        $students->email = $request->email;
        $students->status = $request->status;
        $students->gender = $request->gender;
        $students->dateofjoin = $request->dateofjoin;
        $students->amount_paid = $request->amount_paid;
        $students->remaining_amount = $request->remaining_amount;
        $courses->save();

        $course_student = new course_student();


        return redirect()->route('students')->with('success',__('tr.student Added successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
